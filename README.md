# Artgorithms

**ART A - Letterism and letter graphics**

![Teapot in a Lava Lamp](./art_a/img/teapot_in_a_lava_lamp_animation.gif)

[_Teapot in a Lava Lamp_ (2022-10-05)](./art_a)

**ART B - Digital Improvization Experiments**

![Coffee Flow](./art_b/coffee_flow_02_thumb.png)

[_Coffee Flow_ (2022-10-15)](./art_b)

**ART C - Algorithmic Rollage**

![0n35h0t](./art_c/0n35h0t_thumb.png)

[_0n35h0t_ (2022-10-16)](./art_c)

**ART D - Function Quantitation**

![Half-Commission](./art_d/half_commission_3.gif)

[_Half-Commission_ (2022-10-27)](./art_d)

**ART E - Generated Graphics**

![Cyberzunk](./art_e/img/cyberzunk_animation.gif)

[_Cyberzunk_ (2022-11-06)](./art_e)

**ART F - Context-free Graphics**

![Volcanic Flake](./art_f/volcanic_flake_final_thumb.png)

[_Volcanic Flake_ (2022-11-10)](./art_f)

**ART G - Fractal Landscape**

![Pandorra](./art_g/renders/pandorra_02_edit_thumb.png)

[_Pandorra_ (2022-11-20)](./art_g/)

**ART H - Non-linear Fractals**

![Wheel](./art_h/wheel_thumb.png)
![Braid](./art_h/braid_thumb.png)

[_Wheel & Braid_ (2022-11-25)](./art_h/)

**ART I - Multidimensional Fractals**

![Fractal Dice](./art_i/final_thumb.png)

[_Fractal Dice_ (2022-12-04)](./art_i/)

**ART J - Strange Attractors**

![A Flower](./art_j/flower_chest_01_thumb.png)

[_A Flower_ (2022-12-10)](./art_j/)

**ART M - Circle Limit**

![A Circle of Gears](./art_m/a_circle_of_gears.png)

[_A Circle of Gears_ (2023-01-14)](./art_m/)

**ART P - Evolutionary Algorithms**

![The Attack of Giant Croissants from Space France](./art_p/the_attack_of_giant_croissants_from_space_france_final_thumb.png)

[_The Attack of Giant Croissants from Space France_ (2022-12-04)](./art_p/)

**ART Q -  Algorithmis Op-art**

![Blades](./art_q/final/blades.png)
![Kanafas](./art_q/final/kanafas.png)

[_Blades & Kanafas_ (2022-12-04)](./art_q/)

**ART R - Digital Collage**

![Neděle](./art_r/nedele_thumb.png)

[_Neděle_ (2022-12-11)](./art_r/)

**ART T - Artistic Stylization**

![Ivora](./art_t/ivora.png)

[_Ivora_ (2023-01-09)](./art_t/)
