# Pandorra

The landscape of a fictitious volcanic moon.

* All worlds were painted and converted into Minecraft saves using [WorldPainter](https://www.worldpainter.net/).
* All renders were rendered using [Chunky](https://chunky-dev.github.io/docs/).
* All renders use the [Sphax PureBDCraft 64x textures](https://bdcraft.net/downloads/purebdcraft-minecraft/).
* All renders feature the [Milky Way panorama by ESO](https://www.eso.org/public/images/eso0932a/).
* All renders were further edited in Photoshop.
* `pandorra_02` and `pandorra_03` feature a landscape made using [World Machine](https://www.world-machine.com/).

(I thank the Minecraft community for all these wonderful tools and for all the fun I had with them over the years.)
