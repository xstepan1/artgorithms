# Neděle

A digital collage of the two things I did today: played Minecraft with my youngest brother and baked some gingerbreads
with my friends.

Galerie: https://app.schoology.com/album/6269564874/content/96872562

I'm not sure I'll manage to upload all the source images. If not, please see https://gitlab.fi.muni.cz/xstepan1/artgorithms/-/tree/master/art_r.
