# Teapot in a Lava Lamp

A short script based on [ASCII Play](https://github.com/ertdfgcvb/play.core). It uses a Utah teapot decimated in Blender and rendered similarly to the ASCII play's [wireframe cube sample](https://play.ertdfgcvb.xyz/#/src/sdf/cube). Finally, the background is just some Perlin noise.

[Source code](https://gitlab.fi.muni.cz/xstepan1/artgorithms/-/tree/master/art_a)
