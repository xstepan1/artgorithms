// Author: cafour
// JS Perlin noise: https://github.com/joeiddon/perlin
// play.core: https://github.com/ertdfgcvb/play.core
// Utah teapot: https://www.cs.utah.edu/~natevm/newell_teaset/newell_teaset.zip     

import * as v2 from '/src/modules/vec2.js'
import * as v3 from '/src/modules/vec3.js'
import { map } from '/src/modules/num.js'
import { rgb, rgb2hex } from '/src/modules/color.js'
import { exportFrame } from '/src/modules/exportframe.js'

let perlin = {
    rand_vect: function () {
        let theta = Math.random() * 2 * Math.PI;
        return { x: Math.cos(theta), y: Math.sin(theta) };
    },
    dot_prod_grid: function (x, y, vx, vy) {
        let g_vect;
        let d_vect = { x: x - vx, y: y - vy };
        if (this.gradients[[vx, vy]]) {
            g_vect = this.gradients[[vx, vy]];
        } else {
            g_vect = this.rand_vect();
            this.gradients[[vx, vy]] = g_vect;
        }
        return d_vect.x * g_vect.x + d_vect.y * g_vect.y;
    },
    smootherstep: function (x) {
        return 6 * x ** 5 - 15 * x ** 4 + 10 * x ** 3;
    },
    interp: function (x, a, b) {
        return a + this.smootherstep(x) * (b - a);
    },
    seed: function () {
        this.gradients = {};
        this.memory = {};
    },
    get: function (x, y) {
        if (this.memory.hasOwnProperty([x, y]))
            return this.memory[[x, y]];
        let xf = Math.floor(x);
        let yf = Math.floor(y);
        //interpolate
        let tl = this.dot_prod_grid(x, y, xf, yf);
        let tr = this.dot_prod_grid(x, y, xf + 1, yf);
        let bl = this.dot_prod_grid(x, y, xf, yf + 1);
        let br = this.dot_prod_grid(x, y, xf + 1, yf + 1);
        let xt = this.interp(x - xf, tl, tr);
        let xb = this.interp(x - xf, bl, br);
        let v = this.interp(y - yf, xt, xb);
        this.memory[[x, y]] = v;
        return v;
    }
}
perlin.seed();

const cols = 80;
const rows = 45;
const fontSize = 80;

export const settings = {
    renderer: "canvas",
    canvasSize: {
        width: cols * fontSize,
        height: rows * fontSize
    },
    backgroundColor: "black",
    fontSize: `${fontSize}px`,
    fontWeight: 700,
    fontFamily: "Cascadia Mono",
    lineHeight: `${fontSize}px`,
    letterSpacing: "0px",
    once: false
}

const density = ' .-+*iaeo08%#W@Ñ'

// Shorthands
const { vec3 } = v3
const { vec2 } = v2
const { sin, cos, floor, abs, exp, min } = Math

// Box primitive
const l = 0.6
const vertices = [
    vec3(0.556218, 2.398329, -1.307541),
    vec3(0.653007, 2.501320, -1.264775),
    vec3(1.134266, 2.398104, -0.840406),
    vec3(1.169676, 2.508968, -0.812874),
    vec3(1.375723, 2.395326, -0.239232),
    vec3(1.307542, 2.398328, 0.556219),
    vec3(1.264776, 2.501320, 0.653008),
    vec3(0.840407, 2.398104, 1.134267),
    vec3(0.812875, 2.508968, 1.169676),
    vec3(0.234041, 2.398721, 1.389942),
    vec3(-0.556218, 2.398328, 1.307542),
    vec3(-0.542785, 2.481530, 1.292855),
    vec3(-1.134266, 2.398104, 0.840407),
    vec3(-1.169676, 2.508968, 0.812875),
    vec3(-1.375723, 2.395326, 0.239233),
    vec3(-1.307542, 2.398328, -0.556218),
    vec3(-1.264776, 2.501320, -0.653007),
    vec3(-0.840407, 2.398104, -1.134266),
    vec3(-0.239232, 2.395326, -1.375723),
    vec3(-0.812875, 2.508968, -1.169675),
    vec3(-0.604631, 2.491629, 1.338995),
    vec3(-0.357592, 2.432829, -1.455864),
    vec3(0.173500, 2.489253, -1.455286),
    vec3(1.455865, 2.432829, -0.357592),
    vec3(1.455287, 2.489253, 0.173500),
    vec3(0.334357, 2.512257, 1.399440),
    vec3(-1.455865, 2.432829, 0.357593),
    vec3(-1.455287, 2.489253, -0.173500),
    vec3(-1.749978, 1.536079, -0.740695),
    vec3(-1.252092, 1.540477, -1.460645),
    vec3(0.740696, 1.536079, -1.749978),
    vec3(1.460645, 1.540477, -1.252092),
    vec3(1.749978, 1.536078, 0.740696),
    vec3(1.252092, 1.540477, 1.460646),
    vec3(-0.478316, 1.615547, 1.828132),
    vec3(-1.460645, 1.540477, 1.252092),
    vec3(-1.952014, 0.951706, 0.630361),
    vec3(-2.013652, 0.975288, -0.405921),
    vec3(-0.630361, 0.951706, -1.952014),
    vec3(0.165875, 1.152919, -1.994546),
    vec3(1.952014, 0.951706, -0.630361),
    vec3(1.994546, 1.152918, 0.165875),
    vec3(0.630361, 0.951706, 1.952014),
    vec3(-0.668764, 0.804916, 1.907973),
    vec3(-1.245527, 0.622133, 1.563871),
    vec3(-1.563871, 0.622133, -1.245527),
    vec3(0.668764, 0.804917, -1.907973),
    vec3(1.245527, 0.622133, -1.563871),
    vec3(1.907973, 0.804917, 0.668764),
    vec3(1.563871, 0.622133, 1.245527),
    vec3(-1.733564, 0.633240, 1.014884),
    vec3(-1.014884, 0.633241, -1.733564),
    vec3(1.733564, 0.633241, -1.014884),
    vec3(1.014884, 0.633240, 1.733564),
    vec3(-0.101227, 0.523173, 1.917700),
    vec3(-1.917700, 0.523173, -0.101227),
    vec3(0.101227, 0.523174, -1.917700),
    vec3(1.917700, 0.523173, 0.101227),
    vec3(0.715643, 0.137758, 1.326513),
    vec3(0.026632, 0.077673, 1.467451),
    vec3(-1.326513, 0.137758, 0.715643),
    vec3(-1.467451, 0.077673, 0.026632),
    vec3(-0.715643, 0.137758, -1.326513),
    vec3(-0.026632, 0.077673, -1.467451),
    vec3(1.326513, 0.137758, -0.715643),
    vec3(1.467451, 0.077673, -0.026632),
    vec3(-0.097824, -0.004443, 0.701864),
    vec3(1.106480, 0.074149, 0.952555),
    vec3(1.383640, 0.123128, 0.568816),
    vec3(-0.952555, 0.074149, 1.106480),
    vec3(-0.568816, 0.123128, 1.383640),
    vec3(0.105098, -0.004418, -0.754193),
    vec3(-1.106480, 0.074149, -0.952555),
    vec3(-1.383640, 0.123128, -0.568816),
    vec3(0.952555, 0.074149, -1.106480),
    vec3(0.568816, 0.123128, -1.383640),
    vec3(0.042685, 3.147011, -0.259288),
    vec3(0.255541, 3.089212, 0.318125),
    vec3(-0.281029, 3.116815, 0.215770),
    vec3(0.394214, 3.071946, -0.066441),
    vec3(-0.359261, 3.009197, 0.037450),
    vec3(-0.276569, 3.076720, -0.300798),
    vec3(0.176597, 3.043792, -0.345004),
    vec3(-0.176597, 3.043792, 0.345005),
    vec3(0.120654, 2.755298, -0.119208),
    vec3(0.119209, 2.755298, 0.120655),
    vec3(-0.120654, 2.755298, 0.119209),
    vec3(-0.119209, 2.755298, -0.120654),
    vec3(-0.242987, 2.635134, -0.276323),
    vec3(0.276323, 2.635134, -0.242987),
    vec3(0.242987, 2.635134, 0.276324),
    vec3(-0.276323, 2.635134, 0.242988),
    vec3(-0.937177, 2.506957, 0.647341),
    vec3(-0.226539, 2.430362, 1.338147),
    vec3(-0.647341, 2.506957, -0.937176),
    vec3(0.937177, 2.506957, -0.647341),
    vec3(0.647341, 2.506956, 0.937177),
    vec3(-1.072450, 2.408247, -0.783248),
    vec3(0.783249, 2.408247, -1.072450),
    vec3(1.072450, 2.408246, 0.783249),
    vec3(-0.783249, 2.408246, 1.072450),
    vec3(-1.222222, 2.403168, 0.519544),
    vec3(-1.338146, 2.430362, -0.226538),
    vec3(-0.519544, 2.403168, -1.222221),
    vec3(0.226539, 2.430362, -1.338146),
    vec3(1.222222, 2.403168, -0.519543),
    vec3(1.338146, 2.430362, 0.226539),
    vec3(0.519544, 2.403168, 1.222222),
    vec3(0.138620, 2.043741, 1.595009),
    vec3(0.000263, 1.982782, 2.470755),
    vec3(0.184127, 2.234674, 1.509908),
    vec3(-0.201374, 2.213855, 1.518101),
    vec3(-0.112995, 2.167574, 2.794541),
    vec3(-0.209068, 1.910382, 2.928830),
    vec3(0.066399, 1.767266, 3.027453),
    vec3(-0.152649, 1.278650, 2.836500),
    vec3(0.001157, 0.596262, 1.896659),
    vec3(0.219211, 0.988254, 2.481844),
    vec3(0.184422, 1.380867, 2.851269),
    vec3(0.233488, 2.047739, 2.800973),
    vec3(0.125633, 2.208230, 2.581571),
    vec3(0.198583, 0.658444, 1.922817),
    vec3(0.151569, 1.325969, 2.592986),
    vec3(0.104874, 0.871761, 2.009489),
    vec3(0.142676, 1.846924, 2.710250),
    vec3(-0.153687, 0.863331, 1.993046),
    vec3(-0.067535, 1.111592, 2.344873),
    vec3(-0.198590, 0.645402, 1.923120),
    vec3(-0.231044, 2.045549, 2.736901),
    vec3(-0.229537, 1.250206, 2.650900),
    vec3(-0.160525, 2.053528, 1.589076),
    vec3(-0.145691, 1.491477, 2.661312),
    vec3(0.417409, 1.333388, -1.699137),
    vec3(0.257747, 1.464799, -2.126006),
    vec3(0.343720, 0.684431, -1.699023),
    vec3(-0.357585, 0.657959, -1.699682),
    vec3(-0.156119, 0.808126, -2.307391),
    vec3(-0.351941, 1.234042, -2.528730),
    vec3(0.145090, 1.281459, -2.631290),
    vec3(0.118253, 2.119057, -2.912596),
    vec3(-0.156693, 2.475735, -3.289419),
    vec3(0.031883, 2.419343, -3.187598),
    vec3(0.040917, 2.476467, -3.433880),
    vec3(0.377162, 0.923030, -2.308397),
    vec3(0.179230, 2.460696, -3.227331),
    vec3(0.424356, 1.314605, -2.450237),
    vec3(0.096320, 2.402725, -2.872777),
    vec3(0.295739, 1.726454, -2.448925),
    vec3(0.104791, 2.465085, -2.895951),
    vec3(-0.008944, 2.377329, -2.816296),
    vec3(-0.002071, 1.680440, -2.325044),
    vec3(-0.309435, 1.477404, -2.287328),
    vec3(-0.037080, 2.274884, -2.559314),
    vec3(-0.086714, 2.395375, -3.159750),
    vec3(-0.228753, 2.140817, -2.771156),
    vec3(-0.485863, 1.091387, -2.267229),
    vec3(-0.365530, 1.333843, -1.699688),
    vec3(-0.068904, 2.464944, -2.833881),
];

const scale = 0.7;
const offset = vec3(0, 0.9, 0);

function preprocess() {
    for (let i = 0; i < vertices.length; i++) {
        let v = v3.mulN(vertices[i], scale);
        // inverse the y axis
        v = v3.mul(v, vec3(1, -1, 1));
        v = v3.add(v, offset);
        vertices[i] = v;
    }
}
preprocess();

const proj = []
const depth = []
const palette = [
    // [172, 22, 44],
    // [255, 215, 0]
    [0, 255, 0]
]

export function pre(context) {
    const t = context.frame * 16.667 * 0.01
    const d = 2
    const zOffs = map(sin(0.12), -1, 1, -2.5, -6);

    for (let i = 0; i < vertices.length; i++) {
        let vt = v3.copy(vertices[i]);
        vt = v3.rotY(vt, context.frame * 0.104)
        depth[i] = map(vt.z, 2.4, -2.4, 0, 1)
        proj[i] = v2.mulN(vec2(vt.x, vt.y), d / (vt.z - zOffs))
    }
}

export function main(coord, context) {
    const t = context.frame * 16.667 * 0.01
    const m = min(context.cols, context.rows)
    const a = context.metrics.aspect

    const st = {
        x: 2.0 * (coord.x - context.cols / 2 + 0.5) / m * a,
        y: 2.0 * (coord.y - context.rows / 2 + 0.5) / m,
    }

    let d = 1e10
    const n = vertices.length
    let currentDepth = 0;
    let currentColor = "black";
    for (let i = 0; i < n; i++) {
        const a = proj[i]
        let newD = v2.length(v2.sub(a, st));
        if (newD < d) {
            d = newD
            currentDepth = depth[i]
            currentColor = palette[i % palette.length];
        }
    }

    const idx = floor(exp(-70 * abs(d)) * density.length)

    if (idx == 0) {
        if (abs(d) > 0.2) {
            let noise = (perlin.get(st.x, st.y - context.frame * 0.01) + 1) / 2 - 1 / abs(d * 10);
            noise = noise - 0.2
            return {
                char: density[Math.round(noise * density.length)],
                color: rgb2hex(rgb(map(noise, -1, 1, 0, 128), 0, 0))
            }
        }
    } else {
        return {
            char: density[idx],
            color: rgb2hex(rgb(currentColor[0] * currentDepth, currentColor[1] * currentDepth, currentColor[2] * currentDepth))
        }
    }
}

import { drawInfo } from '/src/modules/drawbox.js'
export function post(context, cursor, buffer) {
    // drawInfo(context, cursor, buffer)
    exportFrame(context, "export.png", 10, 70);
}
