attribute vec3 aPosition;

void main() {
    vec4 pos = vec4(aPosition, 1.0);
    pos.xy = aPosition.xy * 2.0 - 1.0;
    gl_Position = pos;
}
