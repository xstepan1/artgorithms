# Cyberzunk

This sketch is the answer to the age-old question: _"What happens when you apply almost-Sykora-like™ rules for neighbours onto the symbols from the [FEZ alphabet](https://www.ign.com/wikis/fez/Fez_Alphabet) and make it look kinda like the inventory screen from Cyberpunk 2077?"_

Made using [p5.js](https://p5js.org).

Galerie:
* https://app.schoology.com/album/6269564874/content/96008725
