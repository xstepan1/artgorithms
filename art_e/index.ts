import p5 from "p5";
import { runSketch, Sketch } from "./base";
import { zuLetters, generateGlyphs, Glyph, GlyphSide, loadGrid, zuC, Letter, zuF, rotateGrid, zuB, zuE } from "./glyphs"

const cyberzunk = [
    new Glyph(loadGrid(new Letter("C", 5, zuC))),
    new Glyph(rotateGrid(rotateGrid(rotateGrid(loadGrid(new Letter("Y", 5, zuF)))))),
    new Glyph(loadGrid(new Letter("B", 5, zuB))),
    new Glyph(loadGrid(new Letter("E", 5, zuE))),
    new Glyph(rotateGrid(rotateGrid(loadGrid(new Letter("R", 5, zuF))))),
    new Glyph(rotateGrid(rotateGrid(loadGrid(new Letter("Z", 5, zuE))))),
    new Glyph(rotateGrid(rotateGrid(rotateGrid(loadGrid(new Letter("U", 5, zuC)))))),
    new Glyph(rotateGrid(rotateGrid(loadGrid(new Letter("N", 5, zuB))))),
    new Glyph(rotateGrid(loadGrid(new Letter("K", 5, zuE)))),
]

const bitSize = 10;
const glyphColor = [255, 97, 88, 256];
const dropoutProb = 0.1;
const avgLifespan = 12;

class GlyphInstance {
    constructor(public glyph: Glyph | undefined = undefined, public birth: number = 0) {
    }
}

class MySketch extends Sketch {
    totalWidth: number = -1;
    totalHeight: number = -1;
    gridWidth: number = -1;
    gridHeight: number = -1;
    cellSize: number = -1;
    glyphs: Glyph[] = [];
    grid: GlyphInstance[][] = [];
    margin: number[] = [];
    gradient: p5.Shader = null!;
    gradientTexture: p5.Graphics = null!

    preload() {
        this.gradient = this.p.loadShader("./gradient.vert", "./gradient.frag");
    }

    setup() {
        this.glyphs = generateGlyphs(zuLetters);
        this.cellSize = this.glyphs[0].size * bitSize + bitSize;
        this.margin = [2 * this.cellSize, this.cellSize, this.cellSize, this.cellSize];
        this.gridWidth = this.p.floor((this.p.windowWidth - this.margin[1] - this.margin[3]) / this.cellSize);
        this.gridHeight = this.p.floor((this.p.windowHeight - this.margin[0] - this.margin[2]) / this.cellSize) - 1;

        // NB: these are settings for the screenshots
        // this.gridWidth = 10;
        // this.gridHeight = 10;

        this.grid = [...new Array(this.gridHeight)].map(_ => new Array(this.gridWidth).fill(new GlyphInstance()));
        this.totalWidth = (this.gridWidth + 2) * this.cellSize;
        this.totalHeight = (this.gridHeight + 3) * this.cellSize;

        this.p.createCanvas(this.totalWidth, this.totalHeight, this.p.WEBGL)
        this.p.frameRate(100);
        this.gradientTexture = this.p.createGraphics(this.totalWidth, this.totalHeight, this.p.WEBGL);
        this.gradientTexture.noStroke();
    }

    keyPressed() {
        if (this.p.keyCode === this.p.DOWN_ARROW) {
            this.p.saveCanvas("cyberzunk", "png");
        }
    }

    draw() {
        this.p.clear(0, 0, 0, 0);

        this.drawGradient();

        // NB: This is only usable in P2D mode
        // let context = <CanvasRenderingContext2D>this.p.drawingContext;
        // context.shadowBlur = 16;
        // context.shadowColor = "#ff6158";

        this.drawBorder();

        this.p.noStroke();
        this.p.fill(glyphColor);

        this.reap();

        if (this.p.frameCount % 5 == 0) {
            this.expand(true);
        }

        if (this.p.frameCount % 25 == 0) {
            this.addRandom();
        }

        if (this.p.frameCount % 10 == 0) {
            this.dropout(dropoutProb);
        }

        for (let y = 0; y < this.gridHeight; ++y) {
            for (let x = 0; x < this.gridWidth; ++x) {
                if (this.get(x, y)) {
                    this.drawGridGlyph(this.get(x, y)!, x, y);
                }
            }
        }
    }

    private drawGradient() {
        this.gradientTexture.shader(this.gradient);
        this.gradient.setUniform("resolution", [this.totalWidth, this.totalHeight]);
        this.gradient.setUniform("time", this.p.millis() / 1000.0);
        this.gradientTexture.rect(0, 0, this.totalWidth, this.totalHeight);
        this.p.background(255);
        this.p.texture(this.gradientTexture);
        this.p.translate(-this.totalWidth / 2, -this.totalHeight / 2);
        this.p.rect(0, 0, this.totalWidth, this.totalHeight);
    }

    private drawBorder() {
        this.p.noStroke();

        this.p.fill(glyphColor);
        const borderMargin = [2 * bitSize + this.cellSize, 2 * bitSize, 2 * bitSize, 2 * bitSize];
        const width = this.totalWidth - borderMargin[1] - borderMargin[3];
        const height = this.totalHeight - borderMargin[0] - borderMargin[2];
        // top
        this.p.rect(
            borderMargin[3],
            borderMargin[0],
            width,
            bitSize);
        // right
        this.p.rect(
            borderMargin[3] + width - bitSize,
            borderMargin[0],
            bitSize,
            height);
        // bottom
        this.p.rect(
            borderMargin[3],
            borderMargin[0] + height - bitSize,
            width,
            bitSize);
        // left
        this.p.rect(
            borderMargin[3],
            borderMargin[0],
            bitSize,
            height);

        let smallBit = this.p.floor(bitSize / 2);
        for (let i = 0; i < cyberzunk.length; ++i) {
            let glyph = cyberzunk[cyberzunk.length - i - 1];
            this.drawGlyph(
                glyph,
                this.totalWidth - 2 * bitSize - 6 * smallBit * (i + 1) + smallBit,
                bitSize * 5,
                smallBit);
        }
    }

    private drawGridGlyph(glyph: Glyph, gridX: number, gridY: number, bit = bitSize) {
        this.drawGlyph(
            glyph,
            gridX * this.cellSize + this.margin[3],
            gridY * this.cellSize + this.margin[0],
            bit);
    }

    private drawGlyph(glyph: Glyph, x: number, y: number, bit = bitSize) {
        this.p.fill(glyphColor);
        for (let glyphY = 0; glyphY < glyph.size; ++glyphY) {
            for (let glyphX = 0; glyphX < glyph.size; ++glyphX) {
                if (glyph.grid[glyphY][glyphX] != 1) {
                    continue;
                }
                this.p.rect(
                    x + glyphX * bit,
                    y + glyphY * bit,
                    bit,
                    bit
                );
            }
        }
    }

    private expand(mutate = false) {
        let expanded = [...this.grid].map(e => [...e]);
        for (let y = 0; y < this.gridHeight; ++y) {
            for (let x = 0; x < this.gridWidth; ++x) {
                let cell = this.get(x, y);
                if (cell && !mutate) {
                    this.set(x, y, cell, expanded);
                    continue;
                }

                let { allowds, hasNeighbour } = this.getAllows(x, y);

                if (hasNeighbour && allowds.length != 0) {
                    this.set(x, y, allowds[this.p.floor(this.p.random() * allowds.length)], expanded);
                }
            }
        }
        this.grid = expanded;
    }

    private getAllows(x: number, y: number): { allowds: Glyph[], hasNeighbour: boolean } {
        let allowds = this.glyphs;
        let hasNeighbour = false;
        if (y > 0 && this.get(x, y - 1)) {
            hasNeighbour = true;
            let topsAllowdsForBottom = this.get(x, y - 1)!.allowedNeighbours[GlyphSide.Bottom];
            allowds = allowds.filter(g => topsAllowdsForBottom.includes(g));
        }

        if (y < this.gridHeight - 1 && this.get(x, y + 1)) {
            hasNeighbour = true;
            let bottomsAllowdsForTop = this.get(x, y + 1)!.allowedNeighbours[GlyphSide.Top];
            allowds = allowds.filter(g => bottomsAllowdsForTop.includes(g));
        }

        if (x > 0 && this.get(x - 1, y)) {
            hasNeighbour = true;
            let leftsAllowdsForRight = this.get(x - 1, y)!.allowedNeighbours[GlyphSide.Right];
            allowds = allowds.filter(g => leftsAllowdsForRight.includes(g));
        }

        if (x < this.gridWidth - 1 && this.get(x + 1, y)) {
            hasNeighbour = true;
            let rightsAllowdsForLeft = this.get(x + 1, y)!.allowedNeighbours[GlyphSide.Left];
            allowds = allowds.filter(g => rightsAllowdsForLeft.includes(g));
        }
        return { allowds, hasNeighbour };
    }

    private addRandom() {
        let randX = this.p.floor(this.p.random() * this.gridWidth);
        let randY = this.p.floor(this.p.random() * this.gridHeight);
        let { allowds } = this.getAllows(randX, randY);
        if (allowds.length > 0) {
            let randG = this.p.floor(this.p.random() * allowds.length);
            this.set(randX, randY, allowds[randG]);
        }
    }

    private dropout(prob = 0.05) {
        for (let y = 0; y < this.gridHeight; ++y) {
            for (let x = 0; x < this.gridWidth; ++x) {
                let cell = this.get(x, y);
                if (!cell) {
                    continue;
                }
                if (this.p.random() < prob) {
                    this.set(x, y, undefined);
                }
            }
        }
    }

    private reap() {
        let now = this.p.frameCount;
        for (let y = 0; y < this.gridHeight; ++y) {
            for (let x = 0; x < this.gridWidth; ++x) {
                let random = this.p.round(this.p.random(-5, 5));
                if ((now - this.grid[y][x].birth) > avgLifespan + random) {
                    this.set(x, y, undefined);
                }
            }
        }
    }

    private get(x: number, y: number, grid: GlyphInstance[][] | null = null): Glyph | undefined {
        grid = grid ?? this.grid;
        if (x < 0 || x >= this.gridWidth || y < 0 || y >= this.gridHeight) {
            return undefined;
        }
        return grid[y][x].glyph;
    }

    private set(x: number, y: number, glyph: Glyph | undefined, grid: GlyphInstance[][] | null = null) {
        grid = grid ?? this.grid;
        if (x < 0 || x >= this.gridWidth || y < 0 || y >= this.gridHeight) {
            throw new Error("Out of glyph grid bounds!");
        }
        grid[y][x] = new GlyphInstance(glyph, this.p.frameCount);
    }

    windowResized() {
        this.p.resizeCanvas(this.p.windowWidth, this.p.windowHeight);
    }
}

runSketch(new MySketch());
