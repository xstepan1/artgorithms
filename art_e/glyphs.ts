const zuSize = 5;

export const zuA = `
#####
__#__
#___#
#####
#####`

export const zuB = `
#####
#___#
#_#_#
____#
#####`

export const zuC = `
#####
###__
###_#
###__
#####`

export const zuD = `
#####
#####
#####
__###
#_###`

export const zuE = `
#####
__###
#_###
#____
#####`

export const zuF = `
#####
#_#_#
____#
#_#_#
#####`

export const zu1 = `
##_##
##_##
##_##
#####
#####`

export const zu5 = `
##_##
##_##
___##
#####
#####`

export const zu6 = `
#####
#####
_____
#####
#####`

export const zu8 = `
##_##
##_##
___##
##_##
##_##`

export const zu10 = `
##_##
##_##
_____
##_##
##_##`

export class Letter {
    constructor(public name: string, public size: number, public data: string) {
    }
}

export const zuLetters = [
    new Letter("A", zuSize, zuA),
    new Letter("B", zuSize, zuB),
    new Letter("C", zuSize, zuC),
    new Letter("D", zuSize, zuD),
    new Letter("E", zuSize, zuE),
    new Letter("F", zuSize, zuF),
    new Letter("1", zuSize, zu1),
    new Letter("5", zuSize, zu5),
    // new Letter("6", zuSize, zu6),
    // new Letter("8", zuSize, zu8),
    // new Letter("10", zuSize, zu10),
];

export function createGrid(size: number): number[][] {
    return [...new Array(size)].map(e => new Array(size).fill(0));
}

export function copyGrid(grid: number[][]): number[][] {
    return [...grid].map(e => [...e]);
}

export function loadGrid(letter: Letter): number[][] {
    let current = 0;
    let grid = createGrid(letter.size);
    for (let y = 0; y < letter.size; ++y) {
        for (let x = 0; x < letter.size; ++x) {
            while (current < letter.data.length && letter.data[current] != '#' && letter.data[current] != "_") {
                current++;
            }
            if (current >= letter.data.length) {
                throw new Error("Could not load a letter as it is too short.");
            }
            grid[y][x] = letter.data[current] == '#' ? 1 : 0;
            current++;
        }
    }
    return grid;
}

export function rotateGrid(grid: number[][]): number[][] {
    const size = grid.length;
    let rotatedGrid = createGrid(size);
    let halfSize = Math.floor(size / 2);
    for (let y = 0; y < size; ++y) {
        for (let x = 0; x < size; ++x) {
            let centeredX = x - halfSize;
            let centeredY = y - halfSize;
            let gridX = Math.round(centeredX * Math.cos(Math.PI / 2) - centeredY * Math.sin(Math.PI / 2)) + halfSize;
            let gridY = Math.round(centeredX * Math.sin(Math.PI / 2) + centeredY * Math.cos(Math.PI / 2)) + halfSize;
            rotatedGrid[y][x] = grid[gridY][gridX];
        }
    }
    return rotatedGrid;
}

export enum GlyphSide {
    Top = 0,
    Right = 1,
    Bottom = 2,
    Left = 3
};

export class Glyph {
    public name: string | null = null;
    public originalLetter: Letter | null = null;
    // allowed neighbours for each side: top, right, bottom, left
    public allowedNeighbours: Glyph[][] = [[],[],[],[]];
    public size: number = -1;

    constructor(public grid: number[][]) {
        if (grid.length != grid[0].length) {
            throw new Error("Grids must be square!");
        }
        this.size = grid.length;
    }
}

export function canNeighbour(lhs: Glyph, rhs: Glyph, side: GlyphSide) {
    if (lhs.size != rhs.size) {
        throw new Error("Glyphs must have the same size.");
    }

    const size = lhs.size;
    let isWall = true;
    for (let i = 0; i < size; ++i) {
        let lhsBit = 0;
        let rhsBit = 0;
        switch (side) {
            case GlyphSide.Top:
                lhsBit = lhs.grid[0][i];
                rhsBit = rhs.grid[size - 1][i];
                break;
            case GlyphSide.Right:
                lhsBit = lhs.grid[i][size - 1];
                rhsBit = rhs.grid[i][0];
                break;
            case GlyphSide.Bottom:
                lhsBit = lhs.grid[size - 1][i];
                rhsBit = rhs.grid[0][i];
                break;
            case GlyphSide.Left:
                lhsBit = lhs.grid[i][0];
                rhsBit = rhs.grid[i][size - 1];
                break;
        }

        if (lhsBit != rhsBit) {
            return false;
        }
        
        isWall = isWall && lhsBit == 1 && rhsBit == 1;
    }

    return !isWall;
}

export function generateGlyphs(letters: Letter[]): Glyph[] {
    let glyphs: Glyph[] = [];
    for (let letter of letters) {
        let originalGrid = loadGrid(letter);
        let originalGlyph = new Glyph(originalGrid);
        originalGlyph.name = `${letter.name} (original)`;
        originalGlyph.originalLetter = letter;

        glyphs.push(originalGlyph);
        for (let side = 1; side < 4; ++side) {
            let rotatedGrid = rotateGrid(glyphs[glyphs.length - 1].grid);
            let rotatedGlyph = new Glyph(rotatedGrid);
            rotatedGlyph.name = `${letter.name} (${side}x rotated)`;
            rotatedGlyph.originalLetter = letter;

            glyphs.push(rotatedGlyph);
        }
    }

    for (let i = 0; i < glyphs.length; ++i) {
        let lhs = glyphs[i];
        for (let j = i; j < glyphs.length; ++j) {
            let rhs = glyphs[j];
            for (let side = 0; side < 4; ++side) {
                let allowed = canNeighbour(lhs, rhs, side);
                if (allowed) {
                    lhs.allowedNeighbours[side].push(rhs);
                    rhs.allowedNeighbours[(side + 2) % 4].push(lhs);
                }
            }
        }
    }
    return glyphs;
}
