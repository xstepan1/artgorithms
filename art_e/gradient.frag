precision mediump float;

uniform float time;
uniform vec2 resolution;

vec3 glyphColor = vec3(255.0, 97.0, 88.0);

void main() {
    vec2 offset = vec2(-0.3, -1.0);
    vec2 st = gl_FragCoord.xy / resolution.xy;
    st = st + offset;
    float r = clamp(sqrt(st.x * st.x + st.y * st.y), 0.0, 1.0);
    float phi = atan(st.y / st.x);
    gl_FragColor = vec4((1.0 - r) * glyphColor / 255.0 * 0.3, 1.0);
}
