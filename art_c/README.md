# 0n35h0t

A glitched panorama of a White Carpathian forest with references hidden within.

The JPEG was first compressed as much as possible and then edited manually in the [hex editor of vscode](https://marketplace.visualstudio.com/items?itemName=ms-vscode.hexeditor).


## Other experiments

* `forest_eighth.jpg` -- The first experiment with manual editing of a JPEG file using a hex editor. The image is one eighth of the original size.

* `forest_compressed_01.jpg` -- I replaced all `08` bytes (in the actual image data) with `42` bytes. The result is interesting but unfortunately unreadable.

* `forest_compressed_02.jpg` -- The first attempt at _0n35h0t_. Turns out that it is difficult to both strive for pleasant glitching and hidden references with the raw data. I learnt that it is best to make the changes progressively from top to bottom rather than to jump around randomly. Eventually I gave up on this version and started over.
