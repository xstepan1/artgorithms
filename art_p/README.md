# The Attack of Giant Croissants from Space France

1. The original image was taken by me at the February of this near near Lopenické sedlo.
2. The image was then altered by [Deep Dream Generator](https://deepdreamgenerator.com/) to resemble [The Starry Night](https://en.wikipedia.org/wiki/The_Starry_Night).
3. The giant croissants were painted in by [DALL·E](https://openai.com/dall-e-2/).
    > Huge menacing croissants hovering over a dry meadow and some hills, oil painting.
4. The frame itself was also painted in by [DALL·E](https://openai.com/dall-e-2/) (in two passes).
    > A photo of a painting in a beautiful renaissance frame.
5. The image has been upscaled by an [AI Image Enlarger](https://bigjpg.com/).
6. I manually fixed some artifacts.
