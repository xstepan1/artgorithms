float inv(float x)
{
    if (x == 0.0f) {
        return 0.0f;
    }
    return 1.0f/x;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord/iResolution.xy;
    float x = uv.x - 0.5;
    float y = uv.y - 0.5;
    float first_half = sin(sin(inv(iTime)))
        + inv(x*iTime)
        + sin(y * iTime * 100.0) * x
        + x;
    float second_half = 
        + inv(sin(inv(inv(inv(x))
            + sin(sin(inv(y)*cos(iTime)))*x*y)))*inv(x+sin(x+inv(sin(y))*x*x*sin(iTime)+inv(y))
        + x
        + inv(inv(x)*y)
        + sin(y)*inv(inv(sin(x)))*sin(iTime)*inv(iTime))*inv(x);
    float result = first_half + second_half;
    vec3 col = result * vec3(1, 0, 0) + 0.1 * vec3(0, sin(second_half), cos(first_half));

    // Output to screen
    fragColor = vec4(col,1.0);
}
