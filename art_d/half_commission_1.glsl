float inv(float x)
{
    if (x == 0.0f) {
        return 0.0f;
    }
    return 1.0f/x;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord - (iResolution.xy / 2.0);
    uv = uv / min(iResolution.x, iResolution.y);
    float x = uv.x;
    float y = uv.y;
    //x = iTime * 0.1 * x;
    //y = iTime * 0.01 * y;
    float phi = atan(y, x);
    float r = sqrt(x*x + y*y);
    //x = r;
    //y = phi;
    float first_half = sin(sin(inv(x)))
        + inv(x)
        + sin(iTime) * x
        + x;
    float second_half =
        inv(
            sin(
                inv(
                    inv(
                        inv(x)
                        )
                    +
                    sin(
                        sin(
                            inv(y)*cos(iTime)
                            )
                        )*x*y
                    )
                )
            )
        *
        inv(
            sin(
                inv(
                    sin(y)
                    )*x*x+inv(y)
                )*sin(iTime)
                + inv(inv(x)*y)
                + sin(y)*sin(iTime)
                    *inv(inv(sin(x)))
                    *inv(x*x+y*y)
            )
        *
        inv(x);
    float result = first_half + second_half;
    //result = second_half;
    vec3 col = mod(1.0, result) * vec3(0.2) + vec3(sin(iTime) * 0.5 + 1.0) * 0.3 + vec3(0.05 * sin(second_half));
    if (abs(x*x + y*y) > 0.2)
    {
        col = col + vec3(exp(abs(x*x + y*y)));
    }
    if (abs(x) < 0.04 && abs(y) < 0.04)
    {
        col = 0.69 * vec3(cos(iTime), 0, sin(iTime));
    }

    // Output to screen
    fragColor = vec4(col,1.0);
}
