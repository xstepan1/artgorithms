# Half-Commission

A half-commissioned, half-generated image created using [QuantizeThemAll](https://romop5.github.io/QuantitizeThemAll/), [ShaderToy](https://www.shadertoy.com/), and some client-like input from Nika (hence the half-commission part).

Shadertoy: https://www.shadertoy.com/view/DslGD2

## Other experiments

* `firefox_killer` -- An image of a formula so powerful that its very calculation killed my Firefox.

* `firefox_killer_with_typo` -- The same formula but mistakenly shifted.

Galerie:
* https://app.schoology.com/album/6269564874/content/95664311
* https://app.schoology.com/album/6269564874/content/95664312
