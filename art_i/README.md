# Fractal Dice

Fractals rendered in [Fragmentarium](https://github.com/3Dickulus/FragM), placed into a photo taken by me with shadows added in Photoshop.
