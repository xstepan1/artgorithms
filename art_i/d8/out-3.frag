// Output generated from file: C:/Users/Adam/Downloads/Fragmentarium-2.5.7-221030-winex/Examples/Kaleidoscopic IFS/Octahedron.frag
// Created: Sun Dec 4 18:24:25 2022
#info Octahedron Distance Estimator (Syntopia 2010)
#define providesInit
#include "MathUtils.frag"
#include "DE-Raytracer.frag"
#group Octahedron
// Based on Knighty's Kaleidoscopic IFS 3D Fractals, described here:
// http://www.fractalforums.com/3d-fractal-generation/kaleidoscopic-%28escape-time-ifs%29/

uniform float Scale; slider[0.00,2,4.00]

uniform vec3 Offset; slider[(0,0,0),(1,0,0),(1,1,1)]

uniform float Angle1; slider[-180,0,180]
uniform vec3 Rot1; slider[(-1,-1,-1),(1,1,1),(1,1,1)]
uniform float Angle2; slider[-180,0,180]
uniform vec3 Rot2; slider[(-1,-1,-1),(1,1,1),(1,1,1)]


mat3 fracRotation2;
mat3 fracRotation1;

void init() {
	fracRotation2 = rotationMatrix3(normalize(Rot2), Angle2);
	fracRotation1 = rotationMatrix3(normalize(Rot1), Angle1);
}

// Number of fractal iterations.
uniform int Iterations;  slider[0,13,100]
uniform int ColorIterations;  slider[1,13,20]

// The fractal distance estimation calculation
float DE(vec3 z)
{
	float r;
	
	// Iterate to compute the distance estimator.
	int n = 0;
	while (n < Iterations) {
		z *= fracRotation1;
		
		if (z.x+z.y<0.0) z.xy = -z.yx;
		if (z.x+z.z<0.0) z.xz = -z.zx;
		if (z.x-z.y<0.0) z.xy = z.yx;
		if (z.x-z.z<0.0) z.xz = z.zx;
		
		z = z*Scale - Offset*(Scale-1.0);
		z *= fracRotation2;
		
		r = dot(z, z);
            if (n< ColorIterations)  orbitTrap = min(orbitTrap, abs(vec4(z,r)));
		
		n++;
	}
	
	return (length(z) ) * pow(Scale, -float(n));
}








#preset Default
// Generated by: Octahedron.frag
// Created on: Sun Dec 4 18:24:25 2022
FOV = 0.18118468
Eye = 2.77083561,-0.147160824,5.39725397
Target = -1.58026385,0.086267094,-3.60349444
Up = 0.175292777,0.982664719,-0.059254509
EquiRectangular = false
AutoFocus = false
FocalPlane = 1
Aperture = 0
Gamma = 1.9141324
ToneMapping = 4
Exposure = 1.05360444
Brightness = 1.08208955
Contrast = 0.99264705
AvgLumin = 0.5,0.5,0.5
Saturation = 0.9608209
LumCoeff = 0.2125,0.7154,0.0721
Hue = 0
GaussianWeight = 1
AntiAliasScale = 2
DepthToAlpha = false
ShowDepth = false
DepthMagnitude = 1
Detail = -2.77777773
DetailAO = -0.16485497
FudgeFactor = 1
MaxDistance = 1000
MaxRaySteps = 112
Dither = 0.22807
NormalBackStep = 1
AO = 0,0,0,0.96721
Specular = 1
SpecularExp = 18.8
SpecularMax = 10
SpotLight = 1,1,1,0.17391
SpotLightDir = 0.31428,0.1
CamLight = 1,1,1,0
CamLightMin = 0
Glow = 0.835294,0.0784314,0.0784314,0
GlowMax = 20
Fog = 0
HardShadow = 0.13846
ShadowSoft = 2
QualityShadows = false
Reflection = 0
DebugSun = false
BaseColor = 0,0.333333333,0
OrbitStrength = 0.515
X = 0.333333333,0.666666667,0,0.59056
Y = 0.333333333,0.666666667,0,0.44882
Z = 1,1,1,0.49606
R = 0.666667,0.666667,0.498039,0.07936
BackgroundColor = 0.666667,0.666667,0.498039
GradientBackground = 0.3
CycleColors = false
Cycles = 4.27409
EnableFloor = false
FloorNormal = 0,0,0
FloorHeight = 0
FloorColor = 1,1,1
Scale = 2
Offset = 1,0,0
Angle1 = 0
Rot1 = 1,1,1
Angle2 = 0
Rot2 = 1,1,1
Iterations = 13
ColorIterations = 13
#endpreset

