# Experimenty s digitální improvizací

_Orangeblack_ je pokus se [Silkem](http://weavesilk.com/).

_Cancer in Progress_, _Coffee with Milk_ a _Fractured Latte_ jsou pokusy s [Noize Sparks](noize.surge.sh/) barvou [cosmic latte](https://en.wikipedia.org/wiki/Cosmic_latte) a touhle [kávovou paletou](https://simplicable.com/new/coffee-color).

_Coffee Flow_ je _Cancer in Progress_ prohnaný skrz [Flow Constraint Systems](https://flow.constraint.systems/).
