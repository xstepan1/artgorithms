# A Flower

A flower made in [glChAoS.P](https://github.com/BrutPitt/glChAoS.P) (based off of a `Flower*` sample), upscaled using an [AI Image Enlarger](https://bigjpg.com/), edited in Photoshop to look artistic, and finally slapped onto the side of a chest from Louvre.
