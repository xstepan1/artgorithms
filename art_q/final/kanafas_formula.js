let xorcoord = 29.30;
let xor = Math.round(((Math.round(i / xorcoord) % 3) ^ (Math.round(j / xorcoord) % 3)) / 3);
let sinusoid = ((Math.sin(0.00000007 * i * j * 16) + 1) / 2);
let tangentoid = (Math.pow(Math.tan(6 * j), 2) * Math.pow(Math.tan(6 * i), 2) - 64);
return (xor * sinusoid + (1 - xor) * tangentoid) * 256;
