# Blades & Kanafas

Two op-art experiments made using an old project of mine called _Decks_ (original a sketch for _Generative Design Programming_ at FI MU).

Formula for _Kanafas_:
```js
let xorcoord = 29.30;
let xor = Math.round(((Math.round(i / xorcoord) % 3) ^ (Math.round(j / xorcoord) % 3)) / 3);
let sinusoid = ((Math.sin(0.00000007 * i * j * 16) + 1) / 2);
let tangentoid = (Math.pow(Math.tan(6 * j), 2) * Math.pow(Math.tan(6 * i), 2) - 64);
return (xor * sinusoid + (1 - xor) * tangentoid) * 256;
```

Formula for _Blades_ has unfortunately been lost in a browser crash. However, I think it was something among these lines:
```js
let sinusoid = (Math.sin(0.002 * i * j + 16) + 1) / 2;
let blades = Math.tan(0.088 * (i + j)) + 1 / 2;
return sinusoid * blades * 256;
```

## Galerie

* https://app.schoology.com/album/6269564874/content/96457817
* https://app.schoology.com/album/6269564874/content/96457818
