import { runSketch, Sketch } from "./base";

class DeckOptions {
    funColor = (i: number, j: number) => (i * j) % 256;
    size = 2048;
    margin = 64;
    step = 16;
    spacing = 32;
    cardSize = 64;
    cardCount = 8;
    isBorderVisible = false;
}

class MySketch extends Sketch {
    setup() {
        this.p.resizeCanvas(2048, 2048);
        this.p.clear(255, 255, 255, 255);
        document.getElementById("form")?.addEventListener("keyup", (ev) => {
            if (ev.key === "Enter") {
                this.drawAgain();
            }
        });
        document.getElementById("btn-draw")?.addEventListener("click", () => {
            this.drawAgain();
        });
        document.getElementById("btn-download")?.addEventListener("click", () => {
            this.download();
        });
        this.drawAgain();
    }

    decks(options: DeckOptions) {
        const { funColor, size, margin, step, spacing, cardSize, cardCount, isBorderVisible } = options;
        this.p.resizeCanvas(size, size);
        this.p.background(256);

        if (isBorderVisible) {
            this.p.fill(256);
            this.p.stroke(0);
            this.p.strokeWeight(5);
            this.p.square(0, 0, size);
        }
        this.p.noStroke();

        var relevantSize = size - 2 * margin;
        var deckSize = cardSize + (cardCount - 1) * step;
        var deckCount = this.p.floor(relevantSize / deckSize);
        var spacedDeckSize = deckSize + ((deckCount - 1) * spacing) / deckCount;
        var deckCount = this.p.floor(relevantSize / spacedDeckSize);
        var realSpacing = (relevantSize - deckCount * deckSize) / (deckCount - 1);
        if (deckCount === 0) {
            throw new Error("No decks can be drawn with the given parameters!");
        }

        for (var deckY = 0; deckY < deckCount; ++deckY) {
            for (var deckX = 0; deckX < deckCount; ++deckX) {
                for (var deckZ = 0; deckZ < cardCount; ++deckZ) {
                    var i = deckX * deckCount + deckZ;
                    var j = deckY * deckCount + deckZ;
                    this.p.fill(funColor(i, j));
                    this.p.square(
                        margin + deckX * deckSize + deckX * realSpacing + deckZ * step,
                        margin + deckY * deckSize + deckY * realSpacing + deckZ * step,
                        cardSize);
                }
            }
        }
    }

    drawAgain() {
        var options = new DeckOptions();
        options.funColor = new Function("return (i, j) => {"
            + (<HTMLTextAreaElement>document.getElementById("funColor")).value
            + "}")();
        options.size = this.p.int((<HTMLInputElement>document.getElementById("size")).value);
        options.margin = this.p.int((<HTMLInputElement>document.getElementById("margin")).value);
        options.cardSize = this.p.int((<HTMLInputElement>document.getElementById("cardSize")).value);
        options.step = this.p.int((<HTMLInputElement>document.getElementById("step")).value);
        options.spacing = this.p.int((<HTMLInputElement>document.getElementById("spacing")).value);
        options.cardCount = this.p.int((<HTMLInputElement>document.getElementById("cardCount")).value);
        options.isBorderVisible = (<HTMLInputElement>document.getElementById("isBorderVisible")).checked;
        this.decks(options);
    }
    
    download() {
        this.p.saveCanvas("decks_" + new Date().toISOString().replaceAll(/[:\.]/g, "_"), "png");
    }
    
}

runSketch(new MySketch());
