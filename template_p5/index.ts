import { runSketch, Sketch } from "./base";

class MySketch extends Sketch {
    setup() {
        this.p.resizeCanvas(this.p.windowWidth, this.p.windowHeight);
        this.p.clear(255, 255, 255, 255);
    }

    draw() {
        this.p.fill(0);
        this.p.rect(10, 10, 10, 10);
    }

    windowResized() {
        this.p.resizeCanvas(this.p.windowWidth, this.p.windowHeight);
    }
}

runSketch(new MySketch());
