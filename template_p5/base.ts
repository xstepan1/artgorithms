export { };
import p5 from "p5";

const entryPoints = [
    'setup',
    'draw',
    'preload',
    'deviceMoved',
    'deviceTurned',
    'deviceShaken',
    'doubleClicked',
    'mousePressed',
    'mouseReleased',
    'mouseMoved',
    'mouseDragged',
    'mouseClicked',
    'mouseWheel',
    'touchStarted',
    'touchMoved',
    'touchEnded',
    'keyPressed',
    'keyReleased',
    'keyTyped',
    'windowResized'
];

export abstract class Sketch {
    public p: p5 = null!;
}

export function runSketch(sketch: Sketch, wrapperId = "sketch") {
    let canvas = document.getElementById(wrapperId);
    new p5(p => {
        sketch.p = p;
        for (const entryPoint of entryPoints) {
            if (entryPoint in sketch) {
                Object.defineProperty(p, entryPoint, {
                    value: (<any>sketch)[entryPoint].bind(sketch)
                });
            }
        }
    }, canvas!);
}
