// ------ Camera settings.
// Camera settings. Place these before first rule call.
set translation [-1.22854 1.43483 -20]
set rotation [0.823239 -0.519489 -0.22895 0.101292 0.531231 -0.841151 0.558593 0.669274 0.489951]
set pivot [0 0 0]
set scale 1.16867

// ------ The actual EisenScript
set background #202020

#define shrink 0.85
#define tiny 0.52200635
#define stranslate 2.70863125

{ color #ac162c brightness 1.0} root

rule root {
	box
	{x shrink s shrink} offshoot
	{ry 90 x shrink s shrink} offshoot
	{ry 180 x shrink s shrink} offshoot
	{ry 270 x shrink s shrink} offshoot
	{rz 90 x shrink s shrink } offshoot
	{rz -90 x shrink s shrink } offshoot
}

rule offshoot md 4 {
	axon
	{x stranslate s tiny brightness tiny} nucleus
}

rule axon md 4 {
	box
	{x shrink s shrink} axon
}

rule nucleus {
	box
	{x shrink s shrink} offshoot
	{ry 90 x shrink s shrink} offshoot
	{ry 270 x shrink s shrink} offshoot
	{rz 90 x shrink s shrink} offshoot
	{rz -90 x shrink s shrink} offshoot
}
 


// ----- Settings for internal raytracer

set raytracer::shadows true

// the number of samples controls the quality
// '6' means 6x6 samples per pixels, and is the default.
set raytracer::samples 6

// dof is depth-of-field.
// Use 'Edit | Show 3D Object Information' to find the correct plane 
// comment the line below to disable this.
set raytracer::dof [0.23,0.07]

// Set materials either globally,
// or for a selected tag (e.g. 'shiny')
set raytracer::shiny::reflection 0.3
set raytracer::phong [0.5,0.6,0.2]
set raytracer::size [4096x4096]
 