# Volcanic Flake

A structure generated in [eisenscript editor](https://after12am.github.io/eisenscript-editor/), rendered in [Structure Synth](https://structuresynth.sourceforge.net/), and modified by Kim Asendorf's [ASDFPixelSort](https://github.com/kimasendorf/ASDFPixelSort).
